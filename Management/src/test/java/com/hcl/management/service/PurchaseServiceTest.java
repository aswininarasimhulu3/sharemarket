package com.hcl.management.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.hcl.management.controller.PurchaseController;
import com.hcl.management.dto.PurchaseRequestDto;
import com.hcl.management.exception.ManagementException;
import com.hcl.management.feignclient.PortfolioConnect;
import com.hcl.management.model.Account;
import com.hcl.management.model.Purchase;
import com.hcl.management.repository.AccountRepository;
import com.hcl.management.service.impl.PurchaseServiceImpl;
import com.hcl.portfolio.dto.PortfolioResponseDto;
import com.hcl.portfolio.exception.PortfolioNotFoundException;
@ExtendWith(MockitoExtension.class)
public class PurchaseServiceTest {

		@Mock
		AccountRepository accountRepository;
		@Mock
		PortfolioConnect portfolioConnect;
		
		@InjectMocks
		PurchaseServiceImpl purchaseServiceImpl;

		static PortfolioResponseDto portfolioResponseDto;
		static PurchaseRequestDto purchaseRequestDto;
		static Account account;
		
		@BeforeAll
		public static void setup() {
			account  =new Account();
			account.setAccountId(1);
			account.setAccountNo(12445L);
			account.setAccountType("savings");
			account.setBalance(3000);
						
			portfolioResponseDto=new PortfolioResponseDto();
			portfolioResponseDto.setName("abcd");
			portfolioResponseDto.setPortfolioId(1);
			portfolioResponseDto.setPrice(4000);
			
				

			purchaseRequestDto = new PurchaseRequestDto();
			purchaseRequestDto.setAccountId(1);
			purchaseRequestDto.setPortfolioId(1);
			purchaseRequestDto.setQuantity(10);
			

		
		}

	@Test
	@DisplayName("Purchase: positive scenerio")
	public void purchase() throws PortfolioNotFoundException, ManagementException {
	when(accountRepository.getById(purchaseRequestDto.getAccountId())).thenReturn(account);
	when(portfolioConnect.findById(purchaseRequestDto.getPortfolioId())).thenReturn(portfolioResponseDto);
	//when(purchaseRepository.save(purchase)).thenReturn(purchase);
	assertEquals("Purchased Successfully",purchaseServiceImpl.purchase(purchaseRequestDto));
	}

	@Test
	@DisplayName("Purchase: negative scenerio")
	public void purchase1() throws PortfolioNotFoundException, ManagementException {
	when(accountRepository.getById(purchaseRequestDto.getAccountId())).thenReturn(null);
	assertThrows(ManagementException.class,()-> purchaseServiceImpl.purchase(purchaseRequestDto));
	}

	@Test
	@DisplayName("Purchase: negative scenerio")
	public void purchase2() throws PortfolioNotFoundException, ManagementException {
	when(accountRepository.getById(purchaseRequestDto.getAccountId())).thenReturn(account);
	when(portfolioConnect.findById(purchaseRequestDto.getPortfolioId())).thenThrow(PortfolioNotFoundException.class);

	assertThrows(PortfolioNotFoundException.class,()-> purchaseServiceImpl.purchase(purchaseRequestDto));
	}
}
