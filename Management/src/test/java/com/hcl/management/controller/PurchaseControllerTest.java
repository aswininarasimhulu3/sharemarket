package com.hcl.management.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

import java.util.List;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.hcl.management.dto.AccountResponseDto;
import com.hcl.management.dto.PurchaseRequestDto;
import com.hcl.management.exception.ManagementException;
import com.hcl.management.model.Account;
import com.hcl.management.model.Purchase;
import com.hcl.management.model.User;
import com.hcl.management.service.impl.AccountServiceImpl;
import com.hcl.management.service.impl.PurchaseServiceImpl;
import com.hcl.portfolio.exception.PortfolioNotFoundException;

@ExtendWith(MockitoExtension.class)
public class PurchaseControllerTest {
	@Mock
	PurchaseServiceImpl purchaseServiceImpl;

	@InjectMocks
	PurchaseController purchaseController;

	static PurchaseRequestDto purchaseRequestDto;
//	static Account account;
	static Purchase purchase;

	// static User user;
	// static List<Account> accounts;
	// static List<AccountResponseDto> accountresponses;
	@BeforeAll
	public static void setup() {
		purchase =new Purchase();
		purchase.setId(1);
		purchase.setName("abcd");
		purchase.setPrice(1000);
		purchase.setQuantity(10);
		purchase.setDateTime("08-09-2021 14:42:38");
		

		purchaseRequestDto = new PurchaseRequestDto();
		purchaseRequestDto.setAccountId(1);
		purchaseRequestDto.setPortfolioId(1);
		purchaseRequestDto.setQuantity(10);

		// accountresponses.add(AccountResponseDto);

	}

	@Test
	@DisplayName("Portfolios Purchase:positive Scenario")
	public void purchaseTest() throws ManagementException, PortfolioNotFoundException {
		when(purchaseServiceImpl.purchase(purchaseRequestDto)).thenReturn("Purchased Successfully");
		String result1 = purchaseServiceImpl.purchase(purchaseRequestDto);
		assertEquals("Purchased Successfully", result1);

	}

	@Test
	@DisplayName("Portfolios Purchase:Negative Scenario")
	public void purchaseTestN() throws ManagementException, PortfolioNotFoundException {
		when(purchaseServiceImpl.purchase(purchaseRequestDto)).thenThrow(PortfolioNotFoundException.class);
		assertThrows(PortfolioNotFoundException.class, () -> purchaseController.purchase(purchaseRequestDto));

	}

}
