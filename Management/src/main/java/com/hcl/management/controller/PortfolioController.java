package com.hcl.management.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.management.feignclient.PortfolioConnect;
import com.hcl.portfolio.dto.PortfolioResponseDto;
import com.hcl.portfolio.exception.PortfolioNotFoundException;
@RestController
public class PortfolioController {
	
@Autowired 
PortfolioConnect portfolioConnect;
@GetMapping("/portfolio/{portfolioId}")
public PortfolioResponseDto findById(@PathVariable int portfolioId) throws PortfolioNotFoundException{
	return portfolioConnect.findById(portfolioId);
	
}
@GetMapping("/portfolios/{portfolioName}")
public ResponseEntity<PortfolioResponseDto> findByName(@RequestBody String portfolioName) throws PortfolioNotFoundException{
	return portfolioConnect.findByName(portfolioName);
}

}
