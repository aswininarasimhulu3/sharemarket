package com.hcl.management.dto;



public class PurchaseRequestDto {
	
	private int portfolioId;
	
	private int accountId;
	
	private int quantity;

	public int getPortfolioId() {
		return portfolioId;
	}

	public void setPortfolioId(int portfolioId) {
		this.portfolioId = portfolioId;
	}

	public int getAccountId() {
		return accountId;
	}

	public void setAccountId(int accountId) {
		this.accountId = accountId;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public PurchaseRequestDto(int portfolioId, int accountId, int quantity) {
		super();
		this.portfolioId = portfolioId;
		this.accountId = accountId;
		this.quantity = quantity;
	}

	public PurchaseRequestDto() {
		super();
	}

	
	
}
