package com.hcl.user.exception;

import java.util.Date;

public class ErrorResponse {

		
	private String message;
	private int statusCode;
	private Date localDateTime;
	
	
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public int getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}
	public Date getLocalDateTime() {
		return localDateTime;
	}
	public void setLocalDateTime(Date localDateTime) {
		this.localDateTime = localDateTime;
	}
	
	public ErrorResponse(String message, int statusCode, Date localDateTime) {
		super();
		this.message = message;
		this.statusCode = statusCode;
		this.localDateTime = localDateTime;
	}
	
	public ErrorResponse() {
		super();
	}
	
	
	
}
